<?php get_header(); ?>
<?php
/*
  Template Name: Only Content
*/
 ?>
<?php while(have_posts()): the_post(); ?>
<?php the_content(); ?>
<?php endwhile; ?>
<?php get_footer(); ?>
