<?php $slider = new WP_query(array('post_type'=> 'slide')); ?>
<?php if ($slider->have_posts()): ?>
  <?php while($slider->have_posts()): $slider->the_post(); ?>
  <?php get_template_part('template-part/slide'); ?>
  <?php endwhile; ?>
<?php endif; ?>
