<?php get_header(); ?>
<?php
/*
  Template Name: Page Default
*/
 ?>
<h2><?php the_title() ?></h2>
<span><?php the_excerpt() ?></span>
<hr>
<?php the_content(); ?>
<?php get_footer(); ?>
