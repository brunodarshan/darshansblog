<?php get_header(); ?>
<header>
  <?php get_template_part('slider'); ?>
</header>
<?php if (have_posts()): ?>
  <?php while(have_posts()): the_post();  ?>
    <article class="">
      <?php if (has_post_thumbnail()): ?>
        <?php the_post_thumbnail(); ?>
      <?php endif; ?>
      <h4><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
    </article>
  <?php endwhile; ?>
  <div class="row">
    <div class="pull-left">
      <?php if( get_previous_posts_link() ) :
       previous_posts_link('<- Anteriores', $format_prev);
      endif; ?>
    </div>

    <div class="pull-right">
      <?php if( get_next_posts_link() ) :
      next_posts_link( 'Mais antigos ->', $format_next, 0 );
      endif; ?>
    </div>

  </div>

<?php endif; ?>
<?php get_footer(); ?>
