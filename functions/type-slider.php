<?php
add_action( 'init', 'create_post_type_slide' );

function create_post_type_slide() {

    /**
     * Labels customizados para o tipo de post
     *
     */
    $labels = array(
	    'name' => _x('Slider', 'post type general name'),
	    'singular_name' => _x('Slide', 'post type singular name'),
	    'add_new' => _x('Add New', 'slide'),
	    'add_new_item' => __('Add New Slide'),
	    'edit_item' => __('Edit Slide'),
	    'new_item' => __('New Slide'),
	    'all_items' => __('All Slides'),
	    'view_item' => __('View Slide'),
	    'search_items' => __('Search Slides'),
	    'not_found' =>  __('No Slides found'),
	    'not_found_in_trash' => __('No Slides found in Trash'),
	    'parent_item_colon' => '',
	    'menu_name' => 'Slides'
    );


    register_post_type( 'slide', array(
	    'labels' => $labels,
	    'public' => true,
	    'publicly_queryable' => true,
	    'show_ui' => true,
	    'show_in_menu' => true,
	    'has_archive' => 'films',
	    'rewrite' => array(
		 'slug' => 'films',
		 'with_front' => false,
	    ),
	    'capability_type' => 'post',
	    'has_archive' => false,
	    'hierarchical' => false,
	    'menu_position' => null,
	    'supports' => array('title','thumbnail','excerpt')
	    )
    );





}
