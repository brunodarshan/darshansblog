<?php

function darshanblog_register_menu(){
  register_nav_menu('header_menu', __('Header Menu', 'darshanblog'));
}
function darshanblog_scripts(){
  wp_enqueue_style('main.css', get_template_directory_uri()."/assets/css/main.css");
  wp_enqueue_script('main.js', get_template_directory_uri()."/assets/js/main.js");
}
function darshan_custom_logo() {
      $defaults = array(
          'height'      => 100,
          'width'       => 400,
          'flex-height' => true,
          'flex-width'  => true,
          'header-text' => array( 'site-title', 'site-description' ),
      );
      add_theme_support( 'custom-logo', $defaults );
  }

  add_action('init', 'darshanblog_register');
  add_action('after_setup_theme', 'darshanblog_scripts');
  add_action( 'after_setup_theme', 'darshan_custom_logo' );
  add_theme_support( 'post-thumbnails' );
  require_once "functions/type-slider.php";
