<!DOCTYPE html>
<html lang="<?php bloginfo("language"); ?>">
  <head>
    <meta charset="utf-8">
    <title> <?php bloginfo('name')."-".wp_title();?> </title>
    <?php wp_head(); ?>
  </head>
  <body><?php if ( function_exists( 'the_custom_logo' ) ) {
    the_custom_logo();
} ?>
    <h1><?php bloginfo('name'); ?></h1>
    <?php
    wp_nav_menu(array(
      'menu' => 'Header Menu',
      'theme_location'=> 'header_menu'
    ))
     ?>
